import "./Carousel.css";

const Carousel = () => {
  return (
    <div className="carousel">
      <div className="car-img img-1">
        <img
          src="images/carousel-1.png"
          alt="carousel-images"
        />
      </div>
      <div className="car-img img-2">
        <img
          src="images/carousel-2.png"
          alt="carousel-images"
        />
      </div>
    </div>
  );
};

export default Carousel;
